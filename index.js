'use strict'

const XmlTv = require('./xmltv')
const SkyGuide = require('./skyguide')
const { DateTime } = require('luxon')

/**
 * 
 * @param {Object} opts 
 * @param {number} opts.days
 */
async function getXmlTv(opts) {
    function getId(channelNum) {
        return channelNum.toString().padStart("3", "0")
    }

    const sky = new SkyGuide()
    
    const xml = new XmlTv({
        sourceInfoUrl: 'https://tvguide.sky.co.nz',
        sourceInfoName: 'SKY TV Guide'
    })
    
    let channels = (await sky.getChannels()).filter(c => c.number.match(/^\d\d\d$/))
    xml.addChannels(channels.map(c => ({
        id: getId(c.number),
        displayNames: [
            `${parseInt(c.number)} ${c.name}`,
            c.synopsis.replace('\n', ' ')
        ],
        icon: c.logoThumbnail
    })))

    let now = DateTime.utc()
    for (let i = 0; i < (opts.days || 8); i++) {
        let then = now.plus({ days: 1 })
        xml.addProgrammes(
            (await sky.getGuideData(now.valueOf(), then.valueOf())).events
            .map(p => {
                let categories = p.genres || []
                let title = p.title
                
                // Movies often seem to be prefixed as such
                if (title.startsWith('Movie: ')) {
                    if (!categories.includes('Movie')) categories.push('Movie')
                    title = title.replace(/^Movie: /, '')
                }
                
                // Sometimes the episode number can be extracted from the description
                let episodeNum
                let epNumMatchResult = p.synopsis.match(/S(\d+)\s?Ep?(\d+)/)
                if (epNumMatchResult) {
                    episodeNum = {
                        system: 'xmltv_ns',
                        value: `${parseInt(epNumMatchResult[1]) - 1}.${parseInt(epNumMatchResult[2]) - 1}.0/0`
                    }
                }
                
                // Pull years out of the synopsis for the date
                let date
                let yearMatchResult = p.synopsis.match(/\((\d{4})\)/)
                if (yearMatchResult) {
                    date = yearMatchResult[1]
                }
                if (date && !categories.includes('Movie')) {
                    categories.push('Movie')
                }
    
                // If it's not a movie by now, assume it's a TV show, so set the episode number
                // Ref: https://forums.plex.tv/t/xmltv-categories/197790/9
                let startDate = DateTime.fromMillis(parseInt(p.start)).setZone('Pacific/Auckland')
                let endDate = DateTime.fromMillis(parseInt(p.end)).setZone('Pacific/Auckland')
                if (!episodeNum && !categories.includes('Movie')) {
                    episodeNum = {
                        system: 'original-air-date',
                        value: startDate.toLocal().toFormat('yyyy-MM-dd HH:mm:ss')
                    }
                }
    
                return {
                    channel: getId(p.channelNumber),
                    start: startDate,
                    stop: endDate,
                    rating: p.rating,
                    desc: p.synopsis.replace('\n',' '),
                    categories,
                    title,
                    episodeNum,
                    date
                }
            })
        )

        now = then
    }


    
    return xml.toXml()
}

module.exports = getXmlTv

if (require.main === module) {
    getXmlTv(8).then(x => console.log(x))
}