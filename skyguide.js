'use strict'

const request = require('request-promise')
const debug = require('debug')('xmltv-skynz:skyguide')

class SkyGuide {

    constructor() {
        let req = this.req = request.defaults({
            headers: {
                Accept: 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0.1 Safari/605.1.15',
            }
        })

        this.reqJson = req.defaults({ 
            json: true,
            headers: {
                Referer: 'https://tvguide.sky.co.nz'
            }
        })
    }

    async getChannels() {
        return await this.reqJson('https://static.sky.co.nz/sky/json/channels.prod.json')
    }

    async getGuideData(startTimestamp, endTimestamp) {
        debug(`getGuideData(${startTimestamp}, ${endTimestamp})`)
        return await this.reqJson({
            uri: 'https://web-epg.sky.co.nz/prod/epgs/v1',
            qs: { start: startTimestamp, end: endTimestamp, limit: '20000' }
        })  
    }

}

module.exports = SkyGuide