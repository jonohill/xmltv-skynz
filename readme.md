# xmltv-skynz

Produces XMLTV files by grabbing listings from SkyNZ.

## Just give me some data

Run `node index.js` and you will get 8 days worth of data as STDOUT.

## CLI usage

`cli.js` defines a very basic way to fetch raw data, or generate some XML.
Try running `node cli-js -h` to get usage:
```
Usage:
  cli.js channels
  cli.js data <startTimestamp> <endTimestamp> <channelIds>
  cli.js xmltv <days>

Options:
    <startTimestamp>    Start time to get data, number of milliseconds since Unix epoch (i.e. Unix time * 1000)
    <endTimestamp>      End time to get data, number of milliseconds since Unix epoch (i.e. Unix time * 1000)
    <channelIds>        Comma-seperated list of channels to get data for. Try fetching channels first.
    <days>              How many days to retrieve data for    
```

The `channels` and `data` commands will dump the raw data fetched from Sky, whereas the `xmltv` command will generate XMLTV data.

## API Usage

If this is something you need, have a look at `cli.js` which shows you how to call functions in `skyguide.js` and `xmltv.js`

## Limitations

There hasn't been extensive testing of the output, and because it scrapes from the website you shouldn't expect it to be perfect.
There are some very basic tests in `skyguide.test.js`, those are just there to check if things break due to website changes.

This isn't a compliant (XMLTV Grabber)[http://wiki.xmltv.org/index.php/XmltvCapabilities].
It does produce XMLTV data, but you can't invoke it with the standard Grabber flags.
It would be pretty easy to implement this, but it hasn't been done.
