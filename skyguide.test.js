const SkyGuide = require('./skyguide')
const debug = require('debug')('xmltv-skynz:skyguide.test')
const { DateTime } = require('luxon')

/** @type {SkyGuide} */
let skyGuide

beforeAll(() => {
    skyGuide = new SkyGuide()
})

test('Fetches at least one channel', async () => {
    jest.setTimeout(10000)
    let channels = await skyGuide.getChannels()
    debug(JSON.stringify(channels))
    expect(channels.length).toBeGreaterThan(0)
})

test('Can fetch some data for channel 1', async () => {
    let now = DateTime.utc()
    let then = now.plus({days: 1})
    let data = await skyGuide.getGuideData(now.valueOf(), then.valueOf(), ['001'])
    debug(JSON.stringify(data))
    expect(data.events.length).toBeGreaterThan(0)
})