'use strict'

const {docopt} = require('docopt')
const SkyGuide = require('./skyguide')
const getXmlTv = require('./index')

const doc = `
Usage:
  cli.js channels
  cli.js data <startTimestamp> <endTimestamp>
  cli.js xmltv <days> [--year-means-movie] [--fake-ep-nums]

Options:
    <startTimestamp>    Start time to get data, number of milliseconds since Unix epoch (i.e. Unix time * 1000)
    <endTimestamp>      End time to get data, number of milliseconds since Unix epoch (i.e. Unix time * 1000)
    <days>              How many days to retrieve data for

`

let args = docopt(doc)

if (args.channels) {
    let skyGuide = new SkyGuide()
    skyGuide.getChannels()
    .then(c => {
        let output = JSON.stringify(c, '  ', 2)
        console.log(output)
    })
}

if (args.data) {
    let skyGuide = new SkyGuide()
    skyGuide.getGuideData(args['<startTimestamp>'], args['<endTimestamp>'].split(','))
    .then(c => {
        let output = JSON.stringify(c, '  ', 2)
        console.log(output)
    })
}

if (args.xmltv) {
    getXmlTv({ 
        days: args['<days>']
    })
    .then(x => console.log(x))
}
